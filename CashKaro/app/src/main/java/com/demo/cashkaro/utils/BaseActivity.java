package com.demo.cashkaro.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.cashkaro.R;
import com.mikepenz.community_material_typeface_library.CommunityMaterial.Icon;
import com.mikepenz.iconics.IconicsDrawable;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by Jagan on 02-06-2016.
 */
public class BaseActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView tvToolbarTitle;

    ProgressDialog mDialogLoadingProgress;

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void openActivity(Class activity){
        startActivity(new Intent(this, activity));
    }

    public void initToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        this.toolbar = toolbar;
    }

    public void initToolbarWithAppBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        this.toolbar = toolbar;
    }

    public void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }else {
            toolbar.setTitle(title);
        }
    }

    public void setToolbarCustomTitle(String title) {
        setToolbarTitle("");
        tvToolbarTitle = (TextView) findViewById(R.id.tv_tb_title);
        tvToolbarTitle.setText(title);
    }

    public void setToolbarSubTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(title);
        }else {
            toolbar.setSubtitle(title);
        }
    }

    public void setToolbarNavigationIcon(int icon){
        switch (icon) {
            case Constants.BACK:
                toolbar.setNavigationIcon(new IconicsDrawable(this)
                        .icon(Icon.cmd_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(18));
                break;
            case Constants.HOME:
                toolbar.setNavigationIcon(new IconicsDrawable(this)
                        .icon(Icon.cmd_menu)
                        .color(Color.WHITE)
                        .sizeDp(18));
                break;
        }

    }

    protected void setStatusBarColor(int color) {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP)
            getWindow().setStatusBarColor(getColorRes(color));
    }

    public boolean isNetworkAvailable(){
        boolean state;
        ConnectivityManager cmg = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cmg.getActiveNetworkInfo();
        state = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        if (state) {
            return true;
        } else {
            Log.i("isNetworkAvailable", " No Internet Connection Available! Please Check your Connection");
            return false;
        }
    }

    public void toast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void snackIt(CoordinatorLayout layout, String msg){
        Snackbar.make(layout, msg, Snackbar.LENGTH_LONG)
                .setDuration(7000)
                .show();
    }

    public int getColorRes(int colorResId){
        return  ContextCompat.getColor(this, colorResId);
    }

    public Drawable getDrawableRes(int drawResId){
        return  ContextCompat.getDrawable(this, drawResId);
    }

    protected void setPrefsInt(String key, int val) {
        BaseUtils.setSharedPrefs(getBaseContext(), key, val);
    }

    protected void setPrefsString(String key, String val) {
        BaseUtils.setSharedPrefs(getBaseContext(), key, val);
    }

    protected String getPrefsString(String key) {
        return BaseUtils.getSharedPrefs(getBaseContext(), key, Constants.PREF_DEFAULT_STRING);
    }

    protected int getPrefsInt(String key) {
        return BaseUtils.getSharedPrefs(getBaseContext(), key, Constants.PREF_DEFAULT_INT);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void closeSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); // hide
    }

    public ProgressDialog getDialogLoadingProgress() {
        return mDialogLoadingProgress;
    }

    public void setDialogLoadingProgress(String msg) {
        mDialogLoadingProgress = ProgressDialog.show(this, "", msg, true);
        mDialogLoadingProgress.setCanceledOnTouchOutside(false);
    }

    public void closeDialogLoadingProgress() {
        if (mDialogLoadingProgress.isShowing()) mDialogLoadingProgress.dismiss();
    }

    //    public static void addFragmentWithAnim(FragmentActivity c, Fragment fragment) {
//        FragmentManager fragmentManager = c.getSupportFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.slide_in_up,0,0, R.anim.slide_out_up);
//        transaction.add(R.id.content_frame, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
//
//    public static void addFragmentWithOutAnim(FragmentActivity c, Fragment fragment) {
//        FragmentManager fragmentManager = c.getSupportFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.add(R.id.content_frame, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle home button pressed
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
