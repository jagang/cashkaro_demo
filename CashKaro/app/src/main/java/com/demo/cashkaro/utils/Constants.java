package com.demo.cashkaro.utils;

/**
 * Created by Jagan on 20-05-2017.
 */

public class Constants {

    public static final boolean IS_TEST_APP = true;

    public static final String PREF_DEFAULT_STRING = "default_pref";
    public static final int PREF_DEFAULT_INT = 0;

    public static final int BACK = 0;
    public static final int HOME = 1;

    public static final String MSG_ERROR_NO_INTERNET = "Check internet connection!";
    public static final String MSG_ERROR = "Network error!";

    public static final String OFFER_IMAGES[] = {
            "http://asset6.ckassets.com/resources/image/slider_images/ck-storepage-v2/1x4/slide1.1497777417.png",
            "http://asset6.ckassets.com/resources/image/slider_images/ck-storepage-v2/1x4/slide2.1497777497.png",
            "http://asset6.ckassets.com/resources/image/slider_images/ck-storepage-v2/1x4/slide3.1497777497.png",
            "http://asset6.ckassets.com/resources/image/slider_images/ck-storepage-v2/1x4/slide4.1497777497.png",
            "http://asset6.ckassets.com/resources/image/slider_images/ck-storepage-v2/1x4/slide1.1497777417.png"};

    public static final String DEAL_LINKS[] = {
            "http://www.amazon.in/",
            "http://www.shopclues.com/",
            "https://www.myntra.com/",
            "http://www.nykaa.com/",
            "https://www.flipkart.com/",
            "http://www.jabong.com/"};


    public static final String EXTRAS_DEAL_TYPE = "extras_deal";
}
