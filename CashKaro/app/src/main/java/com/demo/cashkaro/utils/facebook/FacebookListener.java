package com.demo.cashkaro.utils.facebook;

import com.facebook.AccessToken;

public interface FacebookListener {
  void onFbSignInFail(String errorMessage);

  void onFbSignInSuccess(AccessToken authToken, String userId);

  void onFBSignOut();
}
