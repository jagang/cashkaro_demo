package com.demo.cashkaro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;

import com.demo.cashkaro.utils.BaseActivity;
import com.demo.cashkaro.utils.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.demo.cashkaro.utils.Constants.BACK;
import static com.demo.cashkaro.utils.Constants.EXTRAS_DEAL_TYPE;

public class DetailActivity extends BaseActivity {

    @Bind(R.id.bt_get_deal)
    AppCompatButton mBtGetDeal;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        initToolbar(mToolbar);
        setToolbarNavigationIcon(BACK);

    }

    @OnClick(R.id.bt_get_deal)
    public void onViewClicked() {
        if (isNetworkAvailable()) {
            Intent intent = new Intent(DetailActivity.this, GoToDealActivity.class);
            intent.putExtra(EXTRAS_DEAL_TYPE, getIntent().getStringExtra(EXTRAS_DEAL_TYPE));
            startActivity(intent);
        }else {
            toast(Constants.MSG_ERROR_NO_INTERNET);
        }
    }

}
