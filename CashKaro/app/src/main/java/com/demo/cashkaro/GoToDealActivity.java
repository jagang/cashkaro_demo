package com.demo.cashkaro;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;

import com.demo.cashkaro.utils.BaseActivity;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import im.delight.android.webview.AdvancedWebView;
import im.delight.android.webview.AdvancedWebView.Listener;

import static com.demo.cashkaro.utils.Constants.BACK;
import static com.demo.cashkaro.utils.Constants.DEAL_LINKS;
import static com.demo.cashkaro.utils.Constants.EXTRAS_DEAL_TYPE;
import static com.demo.cashkaro.utils.Constants.OFFER_IMAGES;

public class GoToDealActivity extends BaseActivity implements Listener{

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.web_view_deal)
    AdvancedWebView mWebViewDeal;

    private String mDealUrl = "";
    private Dialog mDealRedirectDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_to_deal);
        ButterKnife.bind(this);

        initToolbar(mToolbar);
        setToolbarNavigationIcon(BACK);

        mWebViewDeal.setListener(this, this);

        HashMap<String, String> mapOfDealLink = new HashMap<>();
        for (int i = 0; i < OFFER_IMAGES.length; i++) {
            mapOfDealLink.put(OFFER_IMAGES[i], DEAL_LINKS[i]);
        }

        String url = mapOfDealLink.get(getIntent().getStringExtra(EXTRAS_DEAL_TYPE));

        toast("Congratulations you have clicked on " + url);
        loadPaymentGateWay(url);

    }

    private void loadPaymentGateWay(String url) {

        mDealRedirectDialog = new Dialog(this);
        mDealRedirectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDealRedirectDialog.setContentView(R.layout.dialog_loading_deal);
        mDealRedirectDialog.setCanceledOnTouchOutside(false);
        mDealRedirectDialog.setCancelable(false);
        mDealRedirectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDealRedirectDialog.show();

        setToolbarCustomTitle(url);

        mWebViewDeal.setVisibility(View.VISIBLE);
        mWebViewDeal.setDesktopMode(false);
        mWebViewDeal.loadUrl(url, true); // true means prevent caching

    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        if (!mDealRedirectDialog.isShowing()) mDealRedirectDialog.show();
    }

    @Override
    public void onPageFinished(String url) {
        mDealRedirectDialog.dismiss();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebViewDeal.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebViewDeal.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mDealRedirectDialog.isShowing()) {
            mDealRedirectDialog.dismiss();
        }else {
            super.onBackPressed();
        }
    }
}
