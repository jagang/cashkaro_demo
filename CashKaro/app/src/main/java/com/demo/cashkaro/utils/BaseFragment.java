package com.demo.cashkaro.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.iconics.IconicsDrawable;

/**
 * Created by Jagan on 27-06-2016.
 */
public class BaseFragment extends Fragment {

    Toolbar toolbar;
    TextView tvToolbarTitle;

    ProgressDialog mDialogLoadingProgress;

    public void initToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

//    public void setToolbarCustomTitle(String title) {
//        tvToolbarTitle = (TextView) getView().findViewById(R.id.tv_tb_title);
//        tvToolbarTitle.setText(title);
//    }

    public void setToolbarNavigationIcon(int icon){
        switch (icon) {
            case Constants.BACK:
                toolbar.setNavigationIcon(new IconicsDrawable(getContext())
                        .icon(CommunityMaterial.Icon.cmd_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(18));
                break;
        }

    }

    public void closeFragment() {
//        getFragmentManager().executePendingTransactions();
//        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        getActivity().getSupportFragmentManager().popBackStackImmediate();
//        getFragmentManager().popBackStack();
    }

    public int getColorRes(int colorResId){
        return  ContextCompat.getColor(getContext(), colorResId);
    }

    public Animation setAnimationRes(int resId){
        return  AnimationUtils.loadAnimation(getContext(), resId);
    }

    public void openActivity(Class activity){
        startActivity(new Intent(getActivity(),activity));
    }

    public void toast(String msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public boolean isNetworkAvailable(){
        boolean state;
        ConnectivityManager cmg = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cmg.getActiveNetworkInfo();
        state = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        if (state) {
            return true;
        } else {
            Log.d("isNetworkAvailable", " No Internet Connection Available! Please Check your Connection");
            return false;
        }
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void setPrefsInt(String key, int val) {
        BaseUtils.setSharedPrefs(getContext(), key, val);
    }

    protected void setPrefsString(String key, String val) {
        BaseUtils.setSharedPrefs(getContext(), key, val);
    }

    protected String getPrefsString(String key) {
        return BaseUtils.getSharedPrefs(getContext(), key, Constants.PREF_DEFAULT_STRING);
    }

    protected int getPrefsInt(String key) {
        return BaseUtils.getSharedPrefs(getContext(), key, Constants.PREF_DEFAULT_INT);
    }

    protected void setStatusBarColor(int resColor) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(getColorRes(resColor));
        }
    }

    protected void vibrate(int duration) {
        Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(duration);
    }

    public void setDialogLoadingProgress(String msg) {
        mDialogLoadingProgress = ProgressDialog.show(getContext(), "", msg, true);
        mDialogLoadingProgress.setCanceledOnTouchOutside(false);
    }

    public void closeDialogLoadingProgress() {
        if (mDialogLoadingProgress.isShowing()) mDialogLoadingProgress.dismiss();
    }

}
