package com.demo.cashkaro;

import android.os.Bundle;
import android.os.Handler;

import com.demo.cashkaro.utils.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openActivity(HomeActivity.class);
                finish();
            }
        }, 2000);

    }

}
