package com.demo.cashkaro;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderLayout.PresetIndicators;
import com.daimajia.slider.library.SliderLayout.Transformer;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.ScaleType;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.demo.cashkaro.utils.BaseActivity;
import com.demo.cashkaro.utils.Constants;
import com.demo.cashkaro.utils.DescriptionHideAnim;
import com.demo.cashkaro.utils.facebook.FacebookHelper;
import com.demo.cashkaro.utils.facebook.FacebookListener;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.demo.cashkaro.utils.Constants.HOME;
import static com.demo.cashkaro.utils.Constants.OFFER_IMAGES;

public class HomeActivity extends BaseActivity implements FacebookListener {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1001;
    private static final String TAG = "HomeActivity";

    @Bind(R.id.carousel_view)
    SliderLayout mIvSlider;
    @Bind(R.id.carousel_view_indicator)
    PagerIndicator mCarouselViewIndicator;
    @Bind(R.id.bt_1)
    AppCompatButton mBt1;
    @Bind(R.id.bt_3)
    AppCompatButton mBt3;
    @Bind(R.id.bt_2)
    AppCompatButton mBt2;
    @Bind(R.id.bt_4)
    AppCompatButton mBt4;
    @Bind(R.id.bt_5)
    AppCompatButton mBt5;
    @Bind(R.id.bt_6)
    AppCompatButton mBt6;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    private FacebookHelper mFacebook;

    private Drawer mDrawer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        initToolbar(mToolbar);
        setupTopCarouselView();
        setupNavigationDrawer();
        setToolbarNavigationIcon(HOME);

        mFacebook = new FacebookHelper(this);
    }

    private void setupTopCarouselView() {

        for (String offer : OFFER_IMAGES) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView.image(offer)
                    .setScaleType(ScaleType.Fit)
                    .setOnSliderClickListener(new OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            /* Analytics */
//                            setAnalytics(getContext(), TAG_ANALYTICS + " BANNER CLICK");

                            Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                            intent.putExtra(Constants.EXTRAS_DEAL_TYPE, slider.getUrl());
                            startActivity(intent);
                        }
                    });
            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle().putString("extra", new Gson().toJson(offer));

            mIvSlider.addSlider(textSliderView);
        }

        mIvSlider.setPresetTransformer(Transformer.Default);
        mIvSlider.setPresetIndicator(PresetIndicators.Center_Top);
        mIvSlider.setCustomIndicator(mCarouselViewIndicator);
        mIvSlider.setCustomAnimation(new DescriptionHideAnim());
        mIvSlider.setDuration(4000);
    }

    private void setupNavigationDrawer() {
        //Create the drawer
        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .inflateMenu(R.menu.nav_menu)
                .withOnDrawerItemClickListener(new OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        switch (position) {
                            case 0:
                                checkPermissionOpenCamera();
                                break;
                            case 1:
                                mFacebook.performSignIn(HomeActivity.this);
                                break;
                        }

                        return false;
                    }
                }).build();
    }

    private void openCamera() {
        startActivity(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean checkPermissionOpenCamera() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Not Granted. Requesting");
            /*ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_STOREGE);*/
            requestPermissions(new String[]{permission.CAMERA}
                    , MY_PERMISSIONS_REQUEST_CAMERA);
            return false;
        } else {
            //Granted
            Log.d(TAG, "Already Granted");
            openCamera();
            return true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult - Fragment");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                Log.d(TAG, "Permission callback");
                // If request is cancelled, the mDrawer arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Already Granted");
                    openCamera();
                } else {
                    Log.d(TAG, "Not Granted");
                    toast("Failed to open Camera. Please provide permission.");
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @OnClick({R.id.bt_1, R.id.bt_3, R.id.bt_2, R.id.bt_4, R.id.bt_5, R.id.bt_6})
    public void onViewClicked(View view) {
        Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
        switch (view.getId()) {
            case R.id.bt_1:
                intent.putExtra(Constants.EXTRAS_DEAL_TYPE, OFFER_IMAGES[0]);
                break;
            case R.id.bt_3:
                intent.putExtra(Constants.EXTRAS_DEAL_TYPE, OFFER_IMAGES[1]);
                break;
            case R.id.bt_2:
                intent.putExtra(Constants.EXTRAS_DEAL_TYPE, OFFER_IMAGES[2]);
                break;
            case R.id.bt_4:
                intent.putExtra(Constants.EXTRAS_DEAL_TYPE, OFFER_IMAGES[3]);
                break;
            case R.id.bt_5:
                intent.putExtra(Constants.EXTRAS_DEAL_TYPE, OFFER_IMAGES[4]);
                break;
            case R.id.bt_6:
                intent.putExtra(Constants.EXTRAS_DEAL_TYPE, OFFER_IMAGES[5]);
                break;
        }
        startActivity(intent);
    }

    @Override
    public void onFbSignInFail(String errorMessage) {
        toast("fb login failed");
    }

    @Override
    public void onFbSignInSuccess(AccessToken authToken, String userId) {
        toast("fb login success");
        setFacebookData(authToken);
    }

    @Override
    public void onFBSignOut() {

    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebook.onActivityResult(requestCode, resultCode, data);
    }

    private void setFacebookData(final AccessToken authToken) {
        GraphRequest request = GraphRequest.newMeRequest(authToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {
                            Log.i("Response",response.toString());

                            String firstName = response.getJSONObject().getString("first_name");

                            toast("Welcome " + firstName);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

}
